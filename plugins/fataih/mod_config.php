<?php

$mod_config = array (
  'mod_name' => 'Forum And Topic Actions In Header',
  'version' => '1.0.17',
  'release_date' => '2019-01-01',
  'author' => 'Otomatic - Fluxbb.fr',
  'mod_installer' => 'OK',
  'fluxbb_versions' => 
  array (
    0 => '1.5.10',
    1 => '1.5.11',
  ),
  'date_install' => 0,
  'mod_status' => 0,
  'mod_abbr' => 'FATAIH',
);

?>