<?php

$mod_config = array (
  'mod_name' => 'No UserList In Navbar',
  'version' => '1.0.16',
  'release_date' => '2019-01.01',
  'author' => 'Otomatic - Fluxbb.fr',
  'mod_installer' => 'OK',
  'fluxbb_versions' => 
  array (
    0 => '1.4.6',
    1 => '1.4.7',
    2 => '1.4.8',
    3 => '1.5.0',
    4 => '1.5.1',
    5 => '1.5.2',
    6 => '1.5.3',
    7 => '1.5.4',
    8 => '1.5.5',
    9 => '1.5.6',
    10 => '1.5.7',
    11 => '1.5.8',
    12 => '1.5.9',
    13 => '1.5.10',
    14 => '1.5.11',
  ),
  'date_install' => 1555685866,
  'mod_status' => 1,
  'mod_abbr' => 'NULIN',
);

?>