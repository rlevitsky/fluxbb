Subject: Добро пожаловать на <board_title>!

Добро пожаловать на форум <base_url>. Учётные данные вашего аккаунта:

Пользователь (username): <username>
Пароль (password): <password>

Пройдите по адресу <login_url>, чтобы активировать аккаунт
(Please click to <login_url> to activate your account).

--
<board_mailer>
(Не отвечайте на это письмо (please don't reply to this message))
